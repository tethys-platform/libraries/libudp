IPv6 RFC: https://datatracker.ietf.org/doc/html/rfc8200
IPv6 Addressing RFC: https://datatracker.ietf.org/doc/html/rfc4291
## IPv4 multicast

| Designation                      | IPv4 notation               |
| -------------------------------- | --------------------------- |
| Local Network Control Block      | 224.0.0.0/24                |
| Internetwork Control Block       | 224.0.1.0/24                |
| Source-Specific Multicast Block  | 232.0.0.0/8                 |
| GLOP Block                       | 233.0.0.0 - 233.251.255.255 |
| Administratively Scoped Block    | 239.0.0.0/8                 |

Addresses in the Internetwork Control Block are used for protocol control traffic that MAY be forwarded through the Internet.  Examples include 224.0.1.1 (Network Time Protocol (NTP) https://datatracker.ietf.org/doc/html/rfc4330) and 224.0.1.68 (`mdhcpdiscover` https://datatracker.ietf.org/doc/html/rfc2730).

SSM https://datatracker.ietf.org/doc/html/rfc4607 is an extension of IP Multicast in which traffic is forwarded to receivers from only those multicast sources for which the receivers have explicitly expressed interest and is primarily targeted at one-to-many (broadcast) applications.

Addresses in the GLOP Block are globally-scoped, statically-assigned addresses.  The assignment is made, for a domain with a 16-bit Autonomous System Number (ASN), by mapping a domain's autonomous system number, expressed in octets as X.Y, into the middle two octets of the GLOP Block, yielding an assignment of 233.X.Y.0/24.

Addresses in the Administratively Scoped Block are for local use within a domain and are described in https://datatracker.ietf.org/doc/html/rfc2365

## IPv6 Address space


| Address type       | Binary prefix      | IPv6 notation |
| ------------------ | ------------------ | ------------- |
| Unspecified        | 00...0  (128 bits) | ::/128        |
| Loopback           | 00...1  (128 bits) | ::1/128       |
| Multicast          | 11111111           | FF00::/8      |
| Link-Local unicast | 1111111010         | FE80::/10     |
| Global Unicast     |(everything else)   | 

Aggregations
- omit leading zeroes
- :: omits zeroes once per address 

Global Unicast 

```
|         n bits         |   m bits  |       128-n-m bits         |
+------------------------+-----------+----------------------------+
| global routing prefix  | subnet ID |       interface ID         |
+------------------------+-----------+----------------------------+
```

All Global Unicast addresses other than those that start with binary 000 have a 64-bit interface ID field

Link-Local 

Link-Local addresses are for use on a single link.  Link-Local addresses have the following format:
```
|   10     |
|  bits    |         54 bits         |          64 bits           |
+----------+-------------------------+----------------------------+
|1111111010|           0             |       interface ID         |
+----------+-------------------------+----------------------------+
```

## IPv6 Multicast

Anycast:   An identifier for a set of interfaces (typically belonging to different nodes).  A packet sent to an anycast address is delivered to one of the interfaces identified by that address (the "nearest" one, according to the routing protocols' measure of distance).

Multicast: An identifier for a set of interfaces (typically belonging to different nodes).  A packet sent to a multicast address is delivered to all interfaces identified by that address.

```
|   8    |  4 |  4 |                  112 bits                   |
+--------+----+----+---------------------------------------------+
|11111111|flgs|scop|                  group ID                   |
+--------+----+----+---------------------------------------------+
```


- `flgs` `|0|R|P|T|`
	- `T = 0` indicates a permanently-assigned ("well-known") multicast address, assigned by the Internet Assigned Numbers Authority (IANA)
    - `T = 1` indicates a non-permanently-assigned ("transient" or "dynamically" assigned) multicast address.
    - The `P` flag's definition and usage can be found in https://datatracker.ietf.org/doc/html/rfc3306
    - The `R` flag's definition and usage can be found in https://datatracker.ietf.org/doc/html/rfc3956

- `scop` is a 4-bit multicast scope value used to limit the scope of the multicast group.  The values are as follows:
	0  reserved
	1  Interface-Local scope
	2  Link-Local scope
	3  reserved
	4  Admin-Local scope
	5  Site-Local scope
	6  (unassigned)
	7  (unassigned)
	8  Organization-Local scope
	9  (unassigned)
	A  (unassigned)
	B  (unassigned)
	C  (unassigned)
	D  (unassigned)
	E  Global scope
	F  reserved
		
	**Interface-Local** scope spans only a single interface on a node and is useful only for loopback transmission of multicast.
	
	**Link-Local** multicast scope spans the same topological region as the corresponding unicast scope.
	
	**Admin-Local** scope is the smallest scope that must be administratively configured, i.e., not automatically derived from physical connectivity or other, non-multicast-related configuration.
	
	**Site-Local** scope is intended to span a single site. 
	
	**Organization-Local** scope is intended to span multiple sites belonging to a single organization.
	
	scopes labeled **"(unassigned)"** are available for administrators to define additional multicast regions.

Reserved Multicast Addresses:   
	`FF00:0:0:0:0:0:0:0 -> FF0F:0:0:0:0:0:0:0`

All Nodes Addresses:    
	`FF01:0:0:0:0:0:0:1`
	`FF02:0:0:0:0:0:0:1 (All devices)`

The above multicast addresses identify the group of all IPv6 nodes,
within scope 1 (interface-local) or 2 (link-local).

All Routers Addresses:   
	`FF01:0:0:0:0:0:0:2`
	`FF02:0:0:0:0:0:0:2`
	`FF05:0:0:0:0:0:0:2`

The above multicast addresses identify the group of all IPv6 routers,
within scope 1 (interface-local), 2 (link-local), or 5 (site-local).

Solicited-Node Address:  `FF02:0:0:0:0:1:FFXX:XXXX`

	Solicited-Node multicast address are computed as a function of a
	node's unicast and anycast addresses.  A Solicited-Node multicast
	address is formed by taking the low-order 24 bits of an address
	(unicast or anycast) and appending those bits to the prefix
	FF02:0:0:0:0:1:FF00::/104

	A node is required to compute and join (on the appropriate interface)
	the associated Solicited-Node multicast addresses for all unicast and
	anycast addresses that have been configured for the node's interfaces
	(manually or automatically).

### Unicast-Prefix-based IPv6 Multicast

RFC: https://datatracker.ietf.org/doc/html/rfc3306

```
|   8    |  4 |  4 |   8    |    8   |       64       |    32    |
+--------+----+----+--------+--------+----------------+----------+
|11111111|flgs|scop|reserved|  plen  | network prefix | group ID |
+--------+----+----+--------+--------+----------------+----------+
```

`flgs` is a set of 4 flags: `|0|0|P|T|`

- `P = 0` indicates a multicast address that is not assigned based on the network prefix.  This indicates a multicast address.
- `P = 1` indicates a multicast address that is assigned based on the network prefix.
- If `P = 1`, `T` MUST be set to 1

The reserved field MUST be zero.

`plen` indicates the actual number of bits in the network prefix field that identify the subnet when `P = 1`.

## API Changes

When using IPv6 address, enclose with `[]`, like so: `[FF02::1]:8080`

Use `getaddrinfo()` to get `struct sockaddr` info (IP version-agnostic)

Hardcoded values considering IP -> helper function to take IPv6 into account

Change `AF_INET` to `AF_INET6`
Change `PF_INET` to `PF_INET6`

Change `INADDR_ANY` assignments to `in6addr_any` assignments

```
struct sockaddr_in sa;
struct sockaddr_in6 sa6;

sa.sin_addr.s_addr = INADDR_ANY;  // use my IPv4 address
sa6.sin6_addr = in6addr_any; // use my IPv6 address
```

Also, the value `IN6ADDR_ANY_INIT` can be used as an initializer when the `struct in6_addr` is declared, like so:

```
struct in6_addr ia6 = IN6ADDR_ANY_INIT;
```

Instead of `struct sockaddr_in` use `struct sockaddr_in6`, being sure to add “6” to the fields as appropriate. There is no `sin6_zero` field.

Instead of `struct in_addr` use `struct in6_addr`, being sure to add “6” to the fields as appropriate

Instead of `inet_aton()` or `inet_addr()`, use `inet_pton()`
Instead of `inet_ntoa()`, use `inet_ntop()`
Instead of `gethostbyname()`, use the superior `getaddrinfo()`
Instead of `gethostbyaddr()`, use the superior `getnameinfo()` (although `gethostbyaddr()` can still work with IPv6)
`INADDR_BROADCAST` no longer works. Use IPv6 multicast instead

## Example

`ff02::1` Well-known Multicast address (All hosts on Link-Local)
`ff12::feed:a:dead:beef` (Transient, Link-Local scope)
`ff15::feed:a:dead:beef` (Transient, Site-Local scope)
`ff18::feed:a:dead:beef` (Transient, Organization-Local scope)

https://www.iana.org/assignments/ipv6-multicast-addresses/ipv6-multicast-addresses.xhtml
List of reserved IPv6 multicast addresses and their uses