use log::info;
use socket2::{Domain, Protocol, SockAddr, Socket, Type};
use std::error::Error;
use std::net::{IpAddr, SocketAddr, SocketAddrV6};
use std::time::Duration;

const IPV4_DEFAULT: &str = "0.0.0.0";
const IPV6_DEFAULT: &str = "0:0:0:0:0:0:0:0";

/// UDP socket config
#[derive(Debug)]
pub struct Config {
    /// Host address
    pub addr: IpAddr,
    /// Host port
    pub port: u16,
    /// Local interface address, used for multicast interface selection
    pub local_addr: IpAddr,
    /// Local interface (v6)
    ///
    /// For interface `0`, `local_addr` has priority for interface selection
    pub local_iface: u32,
    /// Value for the `SO_RCVTIMEO` option on this socket.
    ///
    /// If `read_timeout` is `None`, then `read` and `recv` calls will block
    /// indefinitely.
    pub read_timeout: Option<Duration>,
    /// Value for the `SO_RCVBUF` option on this socket.
    ///
    /// Changes the size of the operating system's receive buffer associated
    /// with the socket.
    pub read_buff_size: Option<usize>,
    /// Value for the SO_SNDBUF option on this socket.
    ///
    /// Changes the size of the operating system's send buffer associated 
    /// with the socket
    pub send_buff_size: Option<usize>,
}

fn brackets(c: char) -> bool {
    c == '[' || c == ']'
}

pub fn interface_from_localaddr(addr: std::net::IpAddr) -> u32 {
    use default_net::interface::*;
    let interfaces = get_interfaces();
    if addr == IPV4_DEFAULT.parse::<IpAddr>().unwrap()
        || addr == IPV6_DEFAULT.parse::<IpAddr>().unwrap()
    {
        return 0;
    }

    match addr {
        IpAddr::V4(addr) => {
            for interface in interfaces {
                if interface.ipv4.iter().find(|net| net.addr == addr).is_some() {
                    return interface.index;
                }
            }
        }
        IpAddr::V6(addr) => {
            for interface in interfaces {
                if interface.ipv6.iter().find(|net| net.addr == addr).is_some() {
                    return interface.index;
                }
            }
        }
    }

    return 0;
}

pub fn interface_from_name(name: &str) -> u32 {
    use default_net::interface::*;
    let interfaces = get_interfaces();
    if let Some(iface) = interfaces.iter().filter(|i| i.name == name).next() {
        iface.index
    } else {
        0
    }
}

pub fn config_from_url(
    url: &str,
    read_timeout: Option<Duration>,
    read_buff_size: Option<usize>,
    send_buff_size: Option<usize>,
) -> Result<Config, Box<dyn Error>> {
    let mut local_iface = 0;
    // Remove %zone_index if exists
    let start_bytes = url.find("[").unwrap_or(url.len());
    let end_bytes = url.find("]").unwrap_or(0);
    let uri = if start_bytes < end_bytes {
        let host = &url[start_bytes..end_bytes];
        if host.contains("%") {
            let zone_index = host.split("%").last().unwrap();
            let new_host = host.replace(&("%".to_owned() + zone_index), "");
            let dezoned = url.replace(host, &new_host);
            match zone_index.parse::<u32>() {
                Ok(ind) => local_iface = ind,
                Err(_) => local_iface = interface_from_name(zone_index),
            }
            dezoned
        } else {
            url.to_owned()
        }
    } else {
        url.to_owned()
    };

    let uri = url::Url::parse_with_params(&uri, &[("localaddr", IPV4_DEFAULT)])?;
    let addr: IpAddr = uri
        .host_str()
        .map(|s| {
            let mut it = s.trim_matches(brackets).split("%");
            let addr = it.next();
            if let Some(iface) = it.next() {
                match iface.parse::<u32>() {
                    Ok(ind) => local_iface = ind,
                    Err(_) => local_iface = interface_from_name(iface),
                }
            }

            addr.unwrap_or(IPV4_DEFAULT).parse()
        })
        .ok_or("UDP address not defined")??;
    let port = uri.port().ok_or("UDP port not defined")?;

    let local_addr: std::net::IpAddr = uri
        .query_pairs()
        .into_owned()
        .find(|s| s.0 == "localaddr")
        .map(|s| s.1.trim_matches(brackets).parse())
        .ok_or("cannot find interface IP")??;

    Ok(Config {
        addr: addr,
        port: port,
        local_addr: local_addr,
        local_iface: local_iface,
        read_timeout: read_timeout,
        read_buff_size: read_buff_size,
        send_buff_size: send_buff_size,
    })
}

/// Opens a UDP socket using URL
///
/// # Examples
///
/// ```
/// use libudp::open_udp_url;
/// use core::time::Duration;
///
/// let socket1 = open_udp_url("udp://224.73.73.73:9786", Some(Duration::new(5, 0)), None, None);
/// let socket2 = open_udp_url("udp://[ff15::15]:9787", None, Some(8 * 1024 * 1024), None);
/// let socket3 = open_udp_url("udp://[ff12::13]:9788?localaddr=2001:db8::13", None, None, None);
/// let socket4 = open_udp_url("udp://[ff11::13%3]:9789", None, None, Some(8 * 1024 * 1024));
/// let socket5 = open_udp_url("udp://[ff11::13%eth0]:9790", None, None, None); // Linux-only
/// ```
pub fn open_udp_url(
    url: &str,
    read_timeout: Option<Duration>,
    read_buff_size: Option<usize>,
    send_buff_size: Option<usize>,
) -> Result<Socket, Box<dyn Error>> {
    let conf = config_from_url(url, read_timeout, read_buff_size, send_buff_size)?;
    open_udp(conf)
}

/// Opens a UDP socket using Config
pub fn open_udp(conf: Config) -> Result<Socket, Box<dyn Error>> {
    let mut client = if cfg!(not(target_os = "linux")) {
        let default = if let IpAddr::V4(_) = conf.addr {
            IPV4_DEFAULT
        } else {
            IPV6_DEFAULT
        };
        SocketAddr::new(default.parse().unwrap(), conf.port)
    } else {
        SocketAddr::new(conf.addr.into(), conf.port)
    };

    let domain = match conf.addr {
        IpAddr::V4(_) => Domain::IPV4,
        IpAddr::V6(_) => Domain::IPV6,
    };

    let socket = Socket::new(domain, Type::DGRAM, Some(Protocol::UDP))?;
    socket.set_read_timeout(conf.read_timeout)?;

    if let Some(size) = conf.read_buff_size {
        if socket.set_recv_buffer_size(size).is_err() {
            info!("Failed setting SO_RCVBUF");
        }
    }

    if let Some(size) = conf.send_buff_size {
        if socket.set_send_buffer_size(size).is_err() {
            info!("Failed setting SO_SNDBUF");
        }
    }

    if conf.addr.is_multicast() {
        match (conf.addr, conf.local_addr) {
            (IpAddr::V4(ref addr_v4), IpAddr::V4(ref localaddr)) => {
                socket.set_multicast_if_v4(localaddr)?;
                socket.join_multicast_v4(addr_v4, localaddr)?;
            }
            (IpAddr::V6(ref addr_v6), _) => {
                let iface = if conf.local_iface == 0 {
                    interface_from_localaddr(conf.local_addr)
                } else {
                    conf.local_iface
                };

                socket.set_multicast_if_v6(iface)?;
                socket.join_multicast_v6(addr_v6, iface)?;
                socket.set_only_v6(true)?;

                let scope = addr_v6.octets()[1] & 0x0f;
                if cfg!(not(target_os = "windows")) && scope == 1 || scope == 2 {
                    if iface == 0 {
                        return Err("must set interface id for link or scope local ipv6 multicast (ff*1::, ff*2::..)".into());
                    }
                    client = SocketAddr::V6(SocketAddrV6::new(*addr_v6, conf.port, 0, iface));
                }
            }
            (IpAddr::V4(_), IpAddr::V6(_)) => {
                return Err("localaddr IpV6, addr IpV4".into());
            }
        }

        socket.set_reuse_address(true)?;
    }

    socket.bind(&SockAddr::from(client))?;

    Ok(socket)
}


pub fn open_udp_for_sending(conf: Config) -> Result<Socket, Box<dyn Error>> {
    let mut client = {
        let default = if let IpAddr::V4(_) = conf.addr {
            IPV4_DEFAULT
        } else {
            IPV6_DEFAULT
        };
        SocketAddr::new(default.parse().unwrap(), conf.port)
    };

    let domain = match conf.addr {
        IpAddr::V4(_) => Domain::IPV4,
        IpAddr::V6(_) => Domain::IPV6,
    };

    let socket = Socket::new(domain, Type::DGRAM, Some(Protocol::UDP))?;

    if let Some(size) = conf.send_buff_size {
        if socket.set_send_buffer_size(size).is_err() {
            info!("Failed setting SO_SNDBUF");
        }
    }

    if conf.addr.is_multicast() {
        match (conf.addr, conf.local_addr) {
            (IpAddr::V4(ref _addr_v4), IpAddr::V4(ref localaddr)) => {
                socket.set_multicast_if_v4(localaddr)?;
            }
            (IpAddr::V6(ref addr_v6), _) => {
                let iface = if conf.local_iface == 0 {
                    interface_from_localaddr(conf.local_addr)
                } else {
                    conf.local_iface
                };

                socket.set_multicast_if_v6(iface)?;
                socket.set_only_v6(true)?;

                let scope = addr_v6.octets()[1] & 0x0f;
                if cfg!(not(target_os = "windows")) && scope == 1 || scope == 2 {
                    if iface == 0 {
                        return Err("must set interface id for link or scope local ipv6 multicast (ff*1::, ff*2::..)".into());
                    }
                    client = SocketAddr::V6(SocketAddrV6::new(*addr_v6, conf.port, 0, iface));
                }
            }
            (IpAddr::V4(_), IpAddr::V6(_)) => {
                return Err("localaddr IpV6, addr IpV4".into());
            }
        }

        socket.set_reuse_address(true)?;
    }

    Ok(socket)
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::mem::{transmute, MaybeUninit};

    #[cfg(test)]
    fn test_conn(url: &str) -> Result<bool, Box<dyn Error>> {
        let mut buf: [MaybeUninit<u8>; 16384] = unsafe { MaybeUninit::uninit().assume_init() };
        let conf = config_from_url(url, None, None, None).unwrap();
        let socket1 = open_udp_url(url, None, None, None).unwrap();
        let socket2 = open_udp_url(url, None, None, None).unwrap();
        let out_addr = std::net::SocketAddr::new(conf.addr, conf.port).into();

        socket1.send_to("test_data".as_bytes(), &out_addr)?;
        let (data_len, _) = socket2.recv_from(&mut buf)?;
        let slice = unsafe { transmute::<&[MaybeUninit<u8>], &[u8]>(&buf[0..data_len]) };

        assert_eq!("test_data".as_bytes(), slice);

        Ok(true)
    }

    #[test]
    fn test_v4_mcast_connection() {
        assert_eq!(test_conn("udp://224.73.73.73:9786").is_err(), false);
    }

    #[test]
    fn test_v4_localaddr() {
        assert_eq!(
            test_conn("udp://224.75.75.75:9786?localaddr=127.0.0.1").is_err(),
            false
        );
    }

    #[test]
    fn test_v6_mcast_connection() {
        assert_eq!(test_conn("udp://[ff15::13]:9786").is_err(), false);
    }

    #[test]
    fn test_v6_localaddr() {
        use default_net::interface::*;
        let interfaces = get_interfaces();
        for iface in interfaces {
            if iface.is_loopback()
                || !iface.is_multicast()
                || !iface.is_up()
                || iface.ipv6.len() == 0
                || (cfg!(not(target_os = "windows")) && iface.flags & 64 == 0)
            {
                continue;
            }

            println!("checking localaddr: {}", iface.name);
            iface
                .ipv6
                .iter()
                .map(|i| {
                    assert_eq!(
                        test_conn(
                            &("udp://[ff11::13]:9786?localaddr=".to_owned() + &i.addr.to_string())
                        )
                        .is_err(),
                        false
                    );
                })
                .collect()
        }
    }

    #[test]
    fn test_v6_scope_name() {
        if cfg!(target_os = "windows") {
            return;
        }

        use default_net::interface::*;
        let interfaces = get_interfaces();
        for iface in interfaces {
            if iface.is_loopback()
                || !iface.is_multicast()
                || !iface.is_up()
                || iface.ipv6.len() == 0
                || (cfg!(not(target_os = "windows")) && iface.flags & 64 == 0)
            {
                continue;
            }

            println!("checking %iface: {}", iface.name);
            assert_eq!(
                test_conn(&("udp://[ff11::15%".to_owned() + &iface.name.to_string() + "]:9786"))
                    .is_err(),
                false
            );
        }
    }

    #[test]
    fn test_v6_scope_index() {
        use default_net::interface::*;
        let interfaces = get_interfaces();
        for iface in interfaces {
            if iface.is_loopback()
                || !iface.is_multicast()
                || !iface.is_up()
                || iface.ipv6.len() == 0
                || (cfg!(not(target_os = "windows")) && iface.flags & 64 == 0)
            {
                continue;
            }

            println!("checking %index: {}", iface.index);
            assert_eq!(
                test_conn(&("udp://[ff11::17%".to_owned() + &iface.index.to_string() + "]:9786"))
                    .is_err(),
                false
            );
        }
    }
}
